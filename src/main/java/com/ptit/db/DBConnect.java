package com.ptit.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnect {

    private static final String HOSTNAME = "localhost";
    private static final String DBNAME = "room_manage?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    public static Connection getConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://"+HOSTNAME+":3306/"+DBNAME,USERNAME,PASSWORD);
        } catch(Exception e) {
            e.printStackTrace();
        }

        return conn;
    }

    public static void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException ignored) {
        }
    }

}
