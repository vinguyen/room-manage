package com.ptit.db;

import com.ptit.constants.DBConfig;
import org.apache.commons.dbcp.BasicDataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPool {

    private static ConnectionPool pool = null;

    private static final BasicDataSource dataSource = new BasicDataSource();

    private ConnectionPool() {
        dataSource.setDriverClassName(DBConfig.DB_DRIVER);
        dataSource.setUrl(DBConfig.CONNECTION_URL);
        dataSource.setUsername(DBConfig.USER_NAME);
        dataSource.setPassword(DBConfig.PASSWORD);
        dataSource.setMinIdle(DBConfig.DB_MIN_CONNECTIONS);
        dataSource.setInitialSize(DBConfig.DB_MIN_CONNECTIONS);
        dataSource.setMaxIdle(DBConfig.DB_MAX_CONNECTIONS);
        dataSource.setMaxOpenPreparedStatements(100);
    }

    public static ConnectionPool getInstance() {
        if(pool==null) {
            pool = new ConnectionPool();
        }
        return pool;
    }

    public Connection getConnection() {
        try {
            return  dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void freeConnection(Connection c) {
        try {
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
