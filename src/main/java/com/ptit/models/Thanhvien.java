package com.ptit.models;

public class Thanhvien {

    private int id;
    private String hodem,ten,ngaysinh, email, sodt, ghichu;
    private Diachi diachi;

    public Thanhvien(String hodem, String ten, String ngaysinh, String email, String sodt, String ghichu, Diachi diachi) {
        this.hodem = hodem;
        this.ten = ten;
        this.ngaysinh = ngaysinh;
        this.email = email;
        this.sodt = sodt;
        this.ghichu = ghichu;
        this.diachi = diachi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHodem() {
        return hodem;
    }

    public void setHodem(String hodem) {
        this.hodem = hodem;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getNgaysinh() {
        return ngaysinh;
    }

    public void setNgaysinh(String ngaysinh) {
        this.ngaysinh = ngaysinh;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSodt() {
        return sodt;
    }

    public void setSodt(String sodt) {
        this.sodt = sodt;
    }

    public String getGhichu() {
        return ghichu;
    }

    public void setGhichu(String ghichu) {
        this.ghichu = ghichu;
    }

    public Diachi getDiachi() {
        return diachi;
    }

    public void setDiachi(Diachi diachi) {
        this.diachi = diachi;
    }
}
