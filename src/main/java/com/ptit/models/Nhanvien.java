package com.ptit.models;

public class Nhanvien {

    private String username, password;
    private Thanhvien thanhvien;

    public Nhanvien(String username, String password, Thanhvien thanhvien) {
        this.username = username;
        this.password = password;
        this.thanhvien = thanhvien;
    }

    public Nhanvien(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Thanhvien getThanhvien() {
        return thanhvien;
    }

    public void setThanhvien(Thanhvien thanhvien) {
        this.thanhvien = thanhvien;
    }
}
