package com.ptit.dao;

import com.ptit.models.Nhanvien;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NhanvienDAO extends DAO {

    public NhanvienDAO() {
        super();
    }

    public boolean kiemtraDangnhap(Nhanvien nhanvien) {
        boolean kq = false;
        PreparedStatement statement=null;
        ResultSet rs = null;

        if(nhanvien.getUsername().contains("true") ||
                nhanvien.getUsername().contains("=")||
                nhanvien.getPassword().contains("true") ||
                nhanvien.getPassword().contains("=")) return false;

        String sql = "SELECT * FROM tblNhanvien WHERE username=? AND password=?";

        if (conn != null) {
            System.out.println("connected...");
            try {
                statement = conn.prepareStatement(sql);

                statement.setString(1, nhanvien.getUsername());
                statement.setString(2, nhanvien.getPassword());
                rs = statement.executeQuery();

                if (rs.next()) {
                    kq = true;
                }

            } catch (SQLException e) {
                try {
                    conn.rollback();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                e.printStackTrace();
            }
            finally {
                pool.freeConnection(conn);
                if (statement!=null) {
                    try {
                        statement.close();
                        if(rs!=null) rs.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return kq;

    }

}
