package com.ptit.dao;

import com.ptit.db.ConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;

public class DAO {

    public static ConnectionPool pool = ConnectionPool.getInstance();
    public static Connection conn;

    public DAO() {
        if (conn == null) {
            ConnectionPool pool = ConnectionPool.getInstance();
            conn = pool.getConnection();
            try{
                if(conn.getAutoCommit())
                    conn.setAutoCommit(false);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

}
